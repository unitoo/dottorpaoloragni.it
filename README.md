www.dottorpaoloragni.it
====================
Main website of Dr. Paolo Ragni, psychologist and psychotherapist.

Developers
-----------
- Claudio Maradonna (claudio@unitoo.pw)

Designers
---------
- Stefano Amandonico (info@grafixprint.it)

License
-------
Licensed under AGPLv3 (see COPYING).

All images are licensed under CC0.

Logo is licensed under CC BY-NC-ND 4.0
